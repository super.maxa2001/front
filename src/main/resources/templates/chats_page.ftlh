<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Chats</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdn.jsdelivr.net/npm/sockjs-client@1/dist/sockjs.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>

<div id="current-user-id" class="d-none">${currentUserId}</div>

<div class="container">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card chat-app">
                <div id="plist" class="people-list">
                    <ul class="list-unstyled chat-list mt-2 mb-0">
                        <#if chats??>
                            <#list chats as contact>
                                <#if currentUserId == contact.applicantId>
                                    <#assign userId =contact.applicantId>
                                    <#assign userUsername = contact.applicantName>
                                    <#assign interlocutorId = contact.companyId>
                                    <#assign interlocutorUsername = contact.hrName>
                                <#else>
                                    <#assign userId = contact.companyId>
                                    <#assign userUsername = contact.hrName>
                                    <#assign interlocutorId = contact.applicantId>
                                    <#assign interlocutorUsername = contact.applicantName>
                                </#if>
                                <li class="clearfix chat-list-item" id="contact-${interlocutorId}">
                                    <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="avatar">
                                    <div class="about">
                                        <div class="name">${interlocutorUsername}</div>
                                    </div>
                                </li>
                            </#list>
                        <#else>
                            You have no chats now
                        </#if>
                    </ul>
                </div>
                <div class="chat" id="chat-partial">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    let stompClient;
    const currentUserId = parseInt(document.getElementById("current-user-id").innerText);
    // const currentUserUsername = document.getElementById("current-user-username").innerText;

    window.onload = async function connect() {
        SockJS = new SockJS("http://localhost:56191/ws");
        stompClient = Stomp.over(SockJS);
        stompClient.connect({}, onConnected, function () {
            alert("error during connection");
        });

        const contactRows = document.getElementsByClassName("chat-list-item");

        for (let i = 0; i < contactRows.length; i++) {
            contactRows[i].addEventListener("click", async function () {
                const url = "/messages/" + contactRows.item(i).id.replace("contact-", "") + "/" + currentUserId;
                let response = await fetch(url);
                document.getElementById("chat-partial").innerHTML = await response.text();
            });
        }
    }

    function onConnected() {
        const destination = "/user/" + currentUserId + "/queue/messages";
        stompClient.subscribe(destination, onMessageReceived);
    }

    function send(recipientId, recipientUsername) {
        let message = document.getElementById("send-message").value;
        sendMessage(message, recipientId, recipientUsername);
        document.getElementById("send-message").value = "";
    }

    function onMessageReceived(message) {
        const msg = JSON.parse(message.body);
        if (msg.senderId.toString() === document.getElementById("other-user-id").innerText) {
            drawMessage(msg.content, new Date(msg.sendDate), msg.senderUsername, false);
            scrollMessages();
        }
    }

    function sendMessage(msg, recipientId, recipientUsername) {
        if (msg.trim() !== "") {
            const message = {
                senderId: currentUserId,
                recipientId: recipientId,
                senderUsername: currentUserUsername,
                recipientUsername: recipientUsername,
                content: msg.trim(),
                sendDate: new Date()
            };
            stompClient.send("/app/chat", {}, JSON.stringify(message));
            drawMessage(message.content, message.sendDate, "You", true);
            scrollMessages();
        }
    }

    function drawMessage(msg, date, sender, isMyMessage) {
        let list = document.getElementById("chat-messages");
        const newMessageBlock = document.createElement("div");
        newMessageBlock.classList.add("pb-4");
        const avatarBlock = document.createElement("div");
        const avatar = document.createElement("img");
        avatar.classList.add("rounded-circle", "mr-1");
        avatar.alt = "avatar";
        avatar.height = 40;
        avatar.width = 40;
        const timeBlock = document.createElement("div");
        timeBlock.classList.add("text-muted", "small", "text-nowrap", "mt-2");
        let messageTime = date;
        const messageValue = document.createElement("div");
        messageValue.classList.add("flex-shrink-1", "bg-light", "rounded", "py-2", "px-3");
        const usernameBlock = document.createElement("div");
        usernameBlock.classList.add("font-weight-bold", "mb-1");
        const contentBlock = document.createElement("div");
        contentBlock.innerText = msg;
        usernameBlock.innerText = sender;

        if (isMyMessage) {
            newMessageBlock.classList.add("chat-message-right");
            avatar.src = document.getElementById("current-user-avatar").innerText;
            messageValue.classList.add("mr-3");
        } else {
            newMessageBlock.classList.add("chat-message-left");
            avatar.src = document.getElementById("other-user-avatar").innerText;
            messageValue.classList.add("ml-3");
        }

        let time = messageTime.getHours() + ":";
        if (messageTime.getMinutes() < 10) {
            time += "0";
        }
        time += messageTime.getMinutes();
        timeBlock.innerText = time;

        avatarBlock.appendChild(avatar);
        avatarBlock.appendChild(timeBlock);
        newMessageBlock.appendChild(avatarBlock);
        messageValue.appendChild(usernameBlock);
        messageValue.appendChild(contentBlock);
        newMessageBlock.appendChild(messageValue);
        list.appendChild(newMessageBlock);
    }

    function scrollMessages() {
        const messagesBlock = document.getElementById("chat-messages");
        messagesBlock.scrollTop = messagesBlock.scrollHeight;
    }
</script>
</body>
<style>
    body {
        background-color: #f4f7f6;
        margin-top: 20px;
    }

    .card {
        background: #fff;
        transition: .5s;
        border: 0;
        margin-bottom: 30px;
        border-radius: .55rem;
        position: relative;
        width: 100%;
        box-shadow: 0 1px 2px 0 rgb(0 0 0 / 10%);
    }

    .chat-app .people-list {
        width: 280px;
        position: absolute;
        left: 0;
        top: 0;
        padding: 20px;
        z-index: 7
    }

    .chat-app .chat {
        height: 600px;
        margin-left: 280px;
        border-left: 1px solid #eaeaea
    }

    .people-list {
        -moz-transition: .5s;
        -o-transition: .5s;
        -webkit-transition: .5s;
        transition: .5s
    }

    .people-list .chat-list li {
        padding: 10px 15px;
        list-style: none;
        border-radius: 3px
    }

    .people-list .chat-list li:hover {
        background: #efefef;
        cursor: pointer
    }

    .people-list .chat-list li.active {
        background: #efefef
    }

    .people-list .chat-list li .name {
        font-size: 15px
    }

    .people-list .chat-list img {
        width: 45px;
        border-radius: 50%
    }

    .people-list img {
        float: left;
        border-radius: 50%
    }

    .people-list .about {
        float: left;
        padding-left: 8px
    }

    .people-list .status {
        color: #999;
        font-size: 13px
    }

    .chat .chat-header {
        padding: 15px 20px;
        border-bottom: 2px solid #f4f7f6
    }

    .chat .chat-header img {
        float: left;
        border-radius: 40px;
        width: 40px
    }

    .chat .chat-header .chat-about {
        float: left;
        padding-left: 10px
    }

    .chat-messages {
        display: flex;
        flex-direction: column;
        min-height: 800px;
        max-height: 800px;
        overflow-y: scroll;
        height: 70vh;
    }

    .chat #chat-partial {
        height: 500px;
    }

    .chat .chat-history {
        display: flex;
        flex-direction: column;
        overflow-y: scroll;
        min-height: 420px;
        height: 420px;
        max-height: 420px;
        padding: 20px;
        border-bottom: 2px solid #fff
    }

    .chat .chat-history ul {
        padding: 0
    }

    .chat .chat-history ul li {
        list-style: none;
        margin-bottom: 30px
    }

    .chat .chat-history ul li:last-child {
        margin-bottom: 0px
    }

    .chat .chat-history .message-data {
        margin-bottom: 15px
    }

    .chat .chat-history .message-data img {
        border-radius: 40px;
        width: 40px
    }

    .chat .chat-history .message-data-time {
        color: #434651;
        padding-left: 6px
    }

    .chat .chat-history .message {
        color: #444;
        padding: 18px 20px;
        line-height: 26px;
        font-size: 16px;
        border-radius: 7px;
        display: inline-block;
        position: relative
    }

    .chat .chat-history .message:after {
        bottom: 100%;
        left: 7%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        border-bottom-color: #fff;
        border-width: 10px;
        margin-left: -10px
    }

    .chat .chat-history .my-message {
        background: #efefef
    }

    .chat .chat-history .my-message:after {
        bottom: 100%;
        left: 30px;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        border-bottom-color: #efefef;
        border-width: 10px;
        margin-left: -10px
    }

    .chat .chat-history .other-message {
        background: #e8f1f3;
        text-align: right
    }

    .chat .chat-history .other-message:after {
        border-bottom-color: #e8f1f3;
        left: 93%
    }

    .chat .chat-message {
        padding: 20px
    }

    .online,
    .offline,
    .me {
        margin-right: 2px;
        font-size: 8px;
        vertical-align: middle
    }

    .online {
        color: #86c541
    }

    .offline {
        color: #e47297
    }

    .me {
        color: #1d8ecd
    }

    .float-right {
        float: right
    }

    .clearfix:after {
        visibility: hidden;
        display: block;
        font-size: 0;
        content: " ";
        clear: both;
        height: 0
    }

    @media only screen and (max-width: 767px) {
        .chat-app .people-list {
            height: 465px;
            width: 100%;
            overflow-x: auto;
            background: #fff;
            left: -400px;
            display: none
        }

        .chat-app .people-list.open {
            left: 0
        }

        .chat-app .chat {
            margin: 0
        }

        .chat-app .chat .chat-header {
            border-radius: 0.55rem 0.55rem 0 0;
            height: 50px
        }

        .chat-app .chat-history {
            height: 300px;
            overflow-x: auto
        }
    }

    @media only screen and (min-width: 768px) and (max-width: 992px) {
        .chat-app .chat-list {
            height: 650px;
            overflow-x: auto
        }

        .chat-app .chat-history {
            height: 600px;
            overflow-x: auto
        }
    }

    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 1) {
        .chat-app .chat-list {
            height: 480px;
            overflow-x: auto
        }

        .chat-app .chat-history {
            height: calc(100vh - 350px);
            overflow-x: auto
        }
    }

</style>
</html>

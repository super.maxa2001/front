package ru.itis.javalab.front.feign;

import ru.itis.javalab.front.models.Chat;
import ru.itis.javalab.front.models.Message;

import java.util.List;

public class ChatFallbackService implements ChatServiceProxy{
    @Override
    public List<Chat> getChats(Long userId) {
        return null;
    }

    @Override
    public Chat getMessagesFromChat(Long companyId, Long applicantId) {
        return null;
    }
}

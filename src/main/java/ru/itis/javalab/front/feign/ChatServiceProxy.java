package ru.itis.javalab.front.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.itis.javalab.front.models.Chat;

import java.util.List;

@FeignClient(name = "offers-connection", fallback = ChatFallbackService.class)
public interface ChatServiceProxy {
    @GetMapping("/messages/{userId}")
    List<Chat> getChats(@PathVariable Long userId);

    @GetMapping("/messages/{companyId}/{applicantId}")
    Chat getMessagesFromChat(@PathVariable Long companyId, @PathVariable Long applicantId);
}

package ru.itis.javalab.front.feign;

import ru.itis.javalab.front.dto.ProfileDto;
import ru.itis.javalab.front.models.Profile;

import java.util.List;

public class ProfileFallbackService implements ProfileServiceProxy{
    @Override
    public List<Profile> getProfiles() {
        return null;
    }

    @Override
    public Profile getProfile(Long id) {
        return null;
    }

    @Override
    public ProfileDto updateProfile(Long id, ProfileDto profileDTO) {
        return null;
    }

    @Override
    public Profile addProfile(ProfileDto profileDto) {
        return null;
    }
}

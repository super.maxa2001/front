package ru.itis.javalab.front.feign;


import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.itis.javalab.front.dto.ProfileDto;
import ru.itis.javalab.front.models.Profile;

import java.util.List;

@FeignClient(name = "profile-connection", fallback = ProfileFallbackService.class)
public interface ProfileServiceProxy {
    @GetMapping("/profile")
    List<Profile> getProfiles();

    @GetMapping("/profile/{id}")
    Profile getProfile(@PathVariable Long id);

    @PostMapping("/profile/{id}")
    ProfileDto updateProfile(@PathVariable Long id, @RequestBody ProfileDto profileDTO);

    @PostMapping("/addProfile")
    Profile addProfile(@RequestBody ProfileDto profileDto);
}

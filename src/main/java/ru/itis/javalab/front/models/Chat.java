package ru.itis.javalab.front.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Chat {
    private Long id;
    private Long applicantId;
    private Long companyId;

    private String applicantName;
    private String hrName;
    private String companyName;
    private List<Message> messages;
}

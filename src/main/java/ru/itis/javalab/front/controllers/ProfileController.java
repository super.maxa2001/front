package ru.itis.javalab.front.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.itis.javalab.front.dto.ProfileDto;
import ru.itis.javalab.front.feign.ProfileServiceProxy;


@Controller
public class ProfileController {

    @Autowired
    private ProfileServiceProxy proxy;

    @GetMapping("/profile/{id}")
    public String getProfilePage(@PathVariable Long id, Model model){
        model.addAttribute("profile", proxy.getProfile(id));
        return "profile_page";
    }

    @PostMapping("/profile/{id}")
    public String updateProfilePage(@PathVariable Long id, @RequestBody ProfileDto profileDTO, Model model){
        model.addAttribute("changeFields", profileDTO);
        model.addAttribute("profileId", id);
        return "update_profile_page";
    }
}

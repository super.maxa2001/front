package ru.itis.javalab.front.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.itis.javalab.front.feign.ChatServiceProxy;

@Controller
public class ChatsController {

    @Autowired
    private ChatServiceProxy proxy;

    @GetMapping("/messages/{userId}")
    public String getChats(@PathVariable Long userId, Model model) {
        model.addAttribute("currentUserId", userId);
        model.addAttribute("chats", proxy.getChats(userId));
        return "chats_page";
    }

    @GetMapping("/messages/{companyId}/{applicantId}")
    public String getChat(@PathVariable Long companyId, @PathVariable Long applicantId, Model model) {
        var chat = proxy.getMessagesFromChat(companyId, applicantId);
        model.addAttribute("companyId", companyId);
        model.addAttribute("applicantId", applicantId);
        model.addAttribute("messages", chat.getMessages());
        model.addAttribute("companyName", chat.getCompanyName());
        return "chat_partial";
    }
}

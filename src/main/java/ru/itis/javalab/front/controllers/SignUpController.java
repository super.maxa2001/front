package ru.itis.javalab.front.controllers;

import org.springframework.web.bind.annotation.GetMapping;

public class SignUpController {

    @GetMapping("sign-up")
    public String getSignUpPage(){
        return "signUp_page";
    }
}
